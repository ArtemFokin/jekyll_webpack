import {
	TimelineLite
} from 'gsap';
import scrollStop from '@/libs/scrollStop';
let ready;

function sendEvent() {
	ready();
	// var event = new CustomEvent('preloaderIsHidden', {})
	// document.dispatchEvent(event);
}

export default function () {

	scrollStop.stop();
	let preloader = document.querySelector('.preloader');
	//красим прелоадер в цвет боди
	let tl = new TimelineLite()
		.to(preloader, .7, {
			opacity: 0
		})
		.add(function () {
			scrollStop.play();
		})
		.set(preloader, {
			display: 'none'
		})
		.add(sendEvent);

	return new Promise(r => ready = r);

};