import device from '@/libs/device.min.js';
/* 
	use data-important-media attribute on Img or Video tags
	for start load run load method and wait promise resolve
*/

export default class ImportantMedia {
	constructor() {
		this.media = document.querySelectorAll('[data-important-media]');
		this.loadedMedia = 0;
	}
	load() {
		if (this.media.length == 0) return new Promise(r => r());

		return new Promise(r => {
			this.ready = r;
			this.startWatch();
		});
	}
	startWatch() {
		this.media.forEach(e => {
			if (e.tagName === 'IMG') {
				if (!e.offsetParent) {
					this.addLoaded();
					return;
				}
				let img = new Image();
				img.onload = img.onerror = () => this.addLoaded(e);
				img.src = e.currentSrc || e.src;
			} else if (e.tagName === 'VIDEO') {
				if (!e.offsetParent) {
					this.addLoaded();
					return;
				}

				let v = document.createElement('video');
				v.oncanplay = v.onerror = () => {
					v.remove();
					this.addLoaded();
					v.oncanplay = function () {};
					v.onerror = function () {};
				}
				v.muted = true;
				v.src = e.src;
				v.play();
				v.style.display = 'none';
				document.body.appendChild(v);
			}
		})
	}
	addLoaded(e) {
		console.log('add loaded: ', e)
		this.loadedMedia++;
		if (this.loadedMedia === this.media.length) this.ready();
	}
}





// function sendEventUpdate() {
// 	var event = new CustomEvent('importantMediaUpdate', {
// 		detail: {
// 			count: loadedMedia,
// 			total: importantMedia.length,
// 		}
// 	})
// 	document.dispatchEvent(event);
// }