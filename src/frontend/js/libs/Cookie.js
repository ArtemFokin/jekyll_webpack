
  export function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              // var cookie = jQuery.trim(cookies[i]);
              var cookie = cookies[i].replace(/^\s+|\s+$/gm,'');
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
  export function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
      var d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
      updatedCookie += "; " + propName;
      var propValue = options[propName];
      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }

    document.cookie = updatedCookie;
  }
  export function deleteCookie(name) {
    this.setCookie(name, "", {
      expires: -1
    })
  }

// function read_cookie_array(name) {
//         var cookies = { };
//         if (document.cookie && document.cookie != '') {
//                 var split = document.cookie.split(';');
//                 for (var i = 0; i < split.length; i++) {
//                         var name_value = split[i].split("=");
//                         name_value[0] = name_value[0].replace(/^ /, '');
//                         name_value[0]=decodeURIComponent(name_value[0]);
//                         name_value[1]=decodeURIComponent(name_value[1]);
//                         if(name_value[0].substring(0,name.length+1)==name+'['){
//                                 name_value[0]=name_value[0].substring(name.length+1,name_value[0].indexOf(']'));
//                                 cookies[name_value[0]] = name_value[1];
//                         }
//                 }
//         }
//         return cookies;
// }
