import './scss/app.scss';
import './js/libs/polyfills';
import './js/libs/detect';

import ImportantMedia from './js/modules/importantMedia';
import preloader from "./js/modules/preloader";


document.addEventListener('DOMContentLoaded', init);

function init() {
	let media = new ImportantMedia();
	let mediaPromise = media.load();
	Promise.all([mediaPromise])
		.then(pageReady);
}


function pageReady() {
	console.log('Page Ready');
	preloader().then(preloaderIsHide);
}

function preloaderIsHide() {

}



// import preloaderInit from './js/modules/preloader';

// watchPageReady();
// startWatchMedia();

// document.addEventListener('pageReady', () => {
// 	preloaderInit();
// })