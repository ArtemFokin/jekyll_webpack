var path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = (env, argv) => {
  var devMode = argv.mode != 'production';
  console.log('dev mode: ', devMode)
  let config = {
    entry: './src/frontend/app.js',
    mode: devMode ?
      'development' : 'production',
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, 'src', 'static')
    },
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          sourceMap: false // set to true if you want JS source maps
        }),
        new OptimizeCSSAssetsPlugin({})
      ]
    },
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "src/frontend/js"),
        // "TweenLite": path.resolve('node_modules', 'gsap/src/minified/TweenLite.js'),
        "TweenMax": path.resolve('node_modules', 'gsap/src/minified/TweenMax.min.js'),
        "ScrollToPlugin": path.resolve('node_modules', 'gsap/src/minified/plugins/ScrollToPlugin.min.js'),
        // "TimelineLite": path.resolve('node_modules',
        // 'gsap/src/minified/TimelineLite.js'),
        "TimelineMax": path.resolve('node_modules', 'gsap/src/minified/TimelineMax.min.js'),
        "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
        "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
        "debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js')
      }
    },
    module: {
      rules: [
        //JS
        {
          test: /\.js$/, //for extension
          exclude: path.resolve(__dirname, 'node_modules'), // path
          use: [{
            loader: "babel-loader",
            options: {
              "presets": [
                [
                  "@babel/preset-env", {
                    "useBuiltIns": "usage"
                  }
                ]
              ]
            }
          }]
        },

        // CSS EXTRACT
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader, {
              loader: 'css-loader',
              options: {
                url: false
              }
            },
            // 'postcss-loader',
            'sass-loader'
          ]
        }
      ]
    },
    plugins: [new MiniCssExtractPlugin('main.css')]
  }

  if (devMode) {
    config.devtool = 'source-map';
  }

  return config;

};