# Jekyll 3.8.5 with Webpack 4

# installation
1. You’ll need to have **Ruby** and **Bundler** installed(https://jekyllrb.com/docs/installation/)
2. Install Ruby dependences `bundle install`
3. Install JS dependences `npm i`

# Run
Run local server `npm run dev`
Build your app `npm run build`

# links
https://jekyllrb.com/
https://jekyllrb.com/tutorials/using-jekyll-with-bundler/
https://webpack.js.org/concepts